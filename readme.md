# OpenSSL Asymmetric Cryptography
Assignment of the 4th laboratory. This program encrypts and decrypts files with public and private keys respectively, using [RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem)) cryptosystem with [AES-128](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) cipher in [CBC](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#CBC) mode of operation.

[EDUX](https://edux.fit.cvut.cz/courses/BI-BEZ/labs/04/start)

## Structure of the encrypted file
| Position from-to (byte index) | Data type           | Value meaning                           |
|-------------------------------|---------------------|-----------------------------------------|
| 0-2                           | 16 bit unsigned int | Initial vector length (ivLen)           |
| 2-4                           | 16 bit unsigned int | Encrypted symmetric key length (keyLen) |
| 4-6                           | 16 bit unsigned int | Cipher NID (OpenSSL cipher ID)          |
| 6-ivLen                       | byte array          | Initial vector                          |
| ivLen-keyLen                  | byte array          | Encrypted symmetric key                 |
| keyLen-EOF (till end of file) | byte array          | Cipher text                             |

## Usage
You can build and run the application via makefile.

### Build
```bash
make
```

### Generate keys
Generates random RSA public and private key pair via openssl.
```bash
make keys
```
Outputs files:
* `pubkey.pem` - Public key
* `privkey.pem` - Private key

### Run
```bash
make run
```
Prints out:
```
usage: bin/out <command>

Commands:
   -e <public_key> <path>   Encrypts file with given public key.
   -d <private_key> <path>  Decrypts file with given private key.
```

### Clean up build files
```
make clean
```

## Return codes
* `0`: Success.
* `1`: File read error.
* `2`: File write error.
* `3`: OpenSSL context error.
* `4`: Decryption error.
* `5`: Encryption error.
* `6`: Public / private key read error.
* `7`: Invalid header (corrupted or invalid file used for decryption).
* `8`: Invalid public / private key file.

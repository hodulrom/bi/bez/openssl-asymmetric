// Roman Hodulák (email: hodulrom@fit.cvut.cz)
#include <cstdlib>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <cstring>
#include <cerrno>
#include <unitypes.h>

// Additional return codes
#define EXIT_FREAD_ERR 1
#define EXIT_FWRITE_ERR 2
#define EXIT_CTX_ERR 3
#define EXIT_OPEN_ERR 4
#define EXIT_SEAL_ERR 5
#define EXIT_KEY_ERR 6
#define EXIT_HEADER_ERR 7
#define EXIT_FKEY_ERR 8

// Commands
#define COMMAND_ENCRYPT "-e"
#define COMMAND_DECRYPT "-d"

// Sizes
#define BYTE sizeof(unsigned char)

// Modes
#define ENCRYPT 1
#define DECRYPT 0

typedef unsigned char byte;
typedef unsigned int uint;

int commandHelp(int argc, char * argv[]) {
	printf("usage: %s <command>\n\n", (argc > 0 ? argv[0] : ""));
	printf("Commands:\n");
	printf("   -e <public_key> <path>   Encrypts file with given public key.\n");
	printf("   -d <private_key> <path>  Decrypts file with given private key.\n");

	return EXIT_SUCCESS;
}

int encrypt(EVP_CIPHER_CTX * ctx, const EVP_CIPHER * cipher, EVP_PKEY * key, FILE * inputFile, FILE * outputFile) {
	// Symmetric key and initial vector
	byte * encryptedKey = new byte[EVP_PKEY_size(key)];
	int encryptedKeyLength = 0;
	byte iv[EVP_MAX_IV_LENGTH];

	if (!EVP_SealInit(ctx, cipher, & encryptedKey, & encryptedKeyLength, iv, & key, 1)) {
		delete[] encryptedKey;
		return EXIT_SEAL_ERR;
	}

	// Header
	uint16_t ivLen = EVP_MAX_IV_LENGTH;
	if (!fwrite(& ivLen, BYTE, 2, outputFile)) {
		delete[] encryptedKey;
		return EXIT_FWRITE_ERR;
	}

	uint16_t keyLen = (uint16_t) encryptedKeyLength;
	if (!fwrite(& keyLen, BYTE, 2, outputFile)) {
		delete[] encryptedKey;
		return EXIT_FWRITE_ERR;
	}

	uint16_t nid = (uint16_t) EVP_CIPHER_nid(cipher);
	if (!fwrite(& nid, BYTE, 2, outputFile)) {
		delete[] encryptedKey;
		return EXIT_FWRITE_ERR;
	}

	if (!fwrite(iv, BYTE, ivLen, outputFile)) {
		delete[] encryptedKey;
		return EXIT_FWRITE_ERR;
	}

	if (!fwrite(encryptedKey, BYTE, keyLen, outputFile)) {
		delete[] encryptedKey;
		return EXIT_FWRITE_ERR;
	}

	// Cipher text
	byte in[1024];
	byte out[sizeof(in) + 1024];
	int outl = 0;

	for (uint inl = (uint) fread(in, BYTE, sizeof(in), inputFile);
		inl > 0 && !ferror(inputFile);
		inl = (uint) fread(in, BYTE, sizeof(in), inputFile)
	) {
		if (!EVP_SealUpdate(ctx, out, & outl, in, inl)) {
			delete[] encryptedKey;
			return EXIT_SEAL_ERR;
		}

		if (outl && !fwrite(out, BYTE, (uint) outl, outputFile)) {
			delete[] encryptedKey;
			return EXIT_FWRITE_ERR;
		}
	}

	// Encryption finalization. This might include some padding in the last block.
	if (!EVP_SealFinal(ctx, out, & outl)) {
		delete[] encryptedKey;
		return EXIT_SEAL_ERR;
	}

	delete[] encryptedKey;

	if (outl && !fwrite(out, BYTE, (uint) outl, outputFile)) {
		return EXIT_FWRITE_ERR;
	}

	if (fflush(outputFile) == EOF) {
		return EXIT_FWRITE_ERR;
	}

	return EXIT_SUCCESS;
}

int decrypt(EVP_CIPHER_CTX * ctx, EVP_PKEY * key, FILE * inputFile, FILE * outputFile) {
	// Symmetric key and initial vector
	byte * encryptedKey = new byte[EVP_PKEY_size(key)];
	byte iv[EVP_MAX_IV_LENGTH];

	// Header
	uint16_t ivLen = 0;
	if (!fread(& ivLen, BYTE, 2, inputFile)) {
		delete[] encryptedKey;
		if (feof(inputFile)) return EXIT_HEADER_ERR;
		return EXIT_FREAD_ERR;
	}

	uint16_t keyLen = 0;
	if (!fread(& keyLen, BYTE, 2, inputFile)) {
		delete[] encryptedKey;
		if (feof(inputFile)) return EXIT_HEADER_ERR;
		return EXIT_FREAD_ERR;
	}

	uint16_t nid = 0;
	if (!fread(& nid, BYTE, 2, inputFile)) {
		delete[] encryptedKey;
		if (feof(inputFile)) return EXIT_HEADER_ERR;
		return EXIT_FREAD_ERR;
	}

	const EVP_CIPHER * cipher = EVP_get_cipherbynid(nid);

	if (cipher == NULL || !ivLen || !keyLen) {
		delete[] encryptedKey;
		return EXIT_HEADER_ERR;
	}

	if (!fread(iv, BYTE, ivLen, inputFile)) {
		delete[] encryptedKey;
		if (feof(inputFile)) return EXIT_HEADER_ERR;
		return EXIT_FREAD_ERR;
	}

	if (!fread(encryptedKey, BYTE, keyLen, inputFile)) {
		delete[] encryptedKey;
		if (feof(inputFile)) return EXIT_HEADER_ERR;
		return EXIT_FREAD_ERR;
	}

	if (!EVP_OpenInit(ctx, cipher, encryptedKey, keyLen, iv, key)) {
		delete[] encryptedKey;
		return EXIT_OPEN_ERR;
	}

	// Cipher text
	byte in[1024];
	byte out[2048];
	int outl = 0;

	for (uint inl = (uint) fread(in, BYTE, sizeof(in), inputFile);
		inl != 0 && !ferror(inputFile);
		inl = (uint) fread(in, BYTE, sizeof(in), inputFile)
	) {
		if (!EVP_OpenUpdate(ctx, out, & outl, in, inl)) {
			delete[] encryptedKey;
			return EXIT_OPEN_ERR;
		}

		if (outl && !fwrite(out, BYTE, (uint) outl, outputFile)) {
			delete[] encryptedKey;
			return EXIT_FWRITE_ERR;
		}
	}

	if (!EVP_OpenFinal(ctx, out, & outl)) {
		delete[] encryptedKey;
		return EXIT_OPEN_ERR;
	}

	delete[] encryptedKey;

	if (outl && !fwrite(out, BYTE, (uint) outl, outputFile)) {
		return EXIT_FWRITE_ERR;
	}

	if (fflush(outputFile) == EOF) {
		return EXIT_FWRITE_ERR;
	}

	return EXIT_SUCCESS;
}

int commandCipher(int argc, char * argv[], int mode) {
	if (argc != 4) {
		return commandHelp(argc, argv);
	}

	const char * decSuffix = ".dec";
	const char * encSuffix = ".enc";
	const char * keyFilename = argv[2];
	const char * inputFilename = argv[3];
	size_t inputFilenameLen = strlen(inputFilename);

	char * outputFilename = new char[inputFilenameLen + 5];
	strcpy(outputFilename, inputFilename);

	if (mode == ENCRYPT) {
		// Rewrite .dec suffix so we don't end up with .dec.enc suffix
		if (strcmp(inputFilename + inputFilenameLen - 4, decSuffix) == 0) {
			strcpy(outputFilename + inputFilenameLen - 4, encSuffix);
			outputFilename[inputFilenameLen] = '\0';
		}
		else {
			strcpy(outputFilename + inputFilenameLen, encSuffix);
			outputFilename[inputFilenameLen + 4] = '\0';
		}
	}
	else {
		// Rewrite .enc suffix so we don't end up with .enc.dec suffix
		if (strcmp(inputFilename + inputFilenameLen - 4, encSuffix) == 0) {
			strcpy(outputFilename + inputFilenameLen - 4, decSuffix);
			outputFilename[inputFilenameLen] = '\0';
		}
		else {
			strcpy(outputFilename + inputFilenameLen, decSuffix);
			outputFilename[inputFilenameLen + 4] = '\0';
		}
	}

	int code;
	EVP_PKEY * key = EVP_PKEY_new();

	do {
		FILE * keyFile = fopen(keyFilename, "rb");

		if (keyFile == NULL) {
			code = EXIT_FKEY_ERR;
			break;
		}

		// We expect unprotected RSA key.
		RSA * rsaKey = (mode == ENCRYPT
			? PEM_read_RSA_PUBKEY(keyFile, NULL, NULL, NULL)
			: PEM_read_RSAPrivateKey(keyFile, NULL, NULL, NULL)
		);

		fclose(keyFile);

		if (rsaKey == NULL || !EVP_PKEY_assign_RSA(key, rsaKey)) {
			code = EXIT_KEY_ERR;
			break;
		}

		FILE * inputFile = fopen(inputFilename, "rb");

		if (inputFile == NULL) {
			code = EXIT_FREAD_ERR;
			break;
		}

		FILE * outputFile = fopen(outputFilename, "wb");

		if (outputFile == NULL) {
			fclose(inputFile);
			code = EXIT_FWRITE_ERR;
			break;
		}

		const EVP_CIPHER * cipher = EVP_aes_128_cbc();
		EVP_CIPHER_CTX * ctx = EVP_CIPHER_CTX_new();

		if (ctx == NULL) {
			code = EXIT_CTX_ERR;
			break;
		}

		OpenSSL_add_all_ciphers();

		code = (mode == ENCRYPT
			? encrypt(ctx, cipher, key, inputFile, outputFile)
			: decrypt(ctx, key, inputFile, outputFile)
		);

		EVP_CIPHER_CTX_free(ctx);
		fclose(inputFile);
		fclose(outputFile);
	}
	while (false);

	switch (code) {
		case EXIT_CTX_ERR:
			fprintf(stderr, "Unable to initialize EVP context\n");
			break;
		case EXIT_OPEN_ERR:
			fprintf(stderr, "EVP open failed: %s\n", ERR_error_string(ERR_get_error(), NULL));
			break;
		case EXIT_SEAL_ERR:
			fprintf(stderr, "EVP seal failed: %s\n", ERR_error_string(ERR_get_error(), NULL));
			break;
		case EXIT_FREAD_ERR:
			fprintf(stderr, "Cannot read file \"%s\": %s\n", inputFilename, strerror(errno));
			break;
		case EXIT_FWRITE_ERR:
			fprintf(stderr, "Cannot write to \"%s\": %s\n", outputFilename, strerror(errno));
			break;
		case EXIT_KEY_ERR:
			fprintf(stderr, "Cannot read key \"%s\": %s\n", keyFilename, ERR_error_string(ERR_get_error(), NULL));
			break;
		case EXIT_FKEY_ERR:
			fprintf(stderr, "Cannot read key \"%s\": %s\n", keyFilename, strerror(errno));
			break;
		case EXIT_HEADER_ERR:
			fprintf(stderr, "Invalid header in file \"%s\"\n", inputFilename);
			break;
		case EXIT_SUCCESS:
			printf("%scrypted to: %s\n", (mode == ENCRYPT ? "En" : "De"), outputFilename);
			break;
	}

	EVP_cleanup();
	CRYPTO_cleanup_all_ex_data();
	ERR_free_strings();
	EVP_PKEY_free(key);
	delete[] outputFilename;

	return code;
}

int commandEncrypt(int argc, char * argv[]) {
	return commandCipher(argc, argv, ENCRYPT);
}

int commandDecrypt(int argc, char * argv[]) {
	return commandCipher(argc, argv, DECRYPT);
}

int main(int argc, char * argv[]) {
	if (argc >= 2) {
		if (strcmp(argv[1], COMMAND_ENCRYPT) == 0) {
			return commandEncrypt(argc, argv);
		}
		else if (strcmp(argv[1], COMMAND_DECRYPT) == 0) {
			return commandDecrypt(argc, argv);
		}
	}

	return commandHelp(argc, argv);
}

outputFile = "bin/out"

all:
	@mkdir -p bin
	g++ --std=c++14 -Wall -pedantic -Wno-long-long -O2 -Werror -lcrypto -o ${outputFile} main.cpp
	@echo "Compiled to \"${outputFile}\""

run:
	@${outputFile} ${args}

clean:
	rm -f astro.bmp.* doge.bmp.* privkey.pem pubkey.pem -r bin

keys:
	openssl genrsa -out privkey.pem 2048
	openssl rsa -in privkey.pem -pubout -out pubkey.pem
